<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lab03 bai2a</title>

    <style>
        div{
            float: left;
            margin-right: 100px;
        }

        .arrow{
            margin-top: 70px;
        }
    </style>
</head>
<body>
    <h1>Convert radians to degrees and vice versa</h1>
    <div>
        <h2>Enter degrees</h2>
        <input id="degree" type="text" placeholder="degree">
    </div>
        <div class="arrow"><=></div>
    <div>
        <h2>Enter radians</h2>
        <input id="radian" type="text" placeholder="radian">
    </div>

    <script>
        const degree = document.getElementById('degree');
        const radian = document.getElementById('radian');

        function convertRadian(degree) {
            if(degree > 360){
                var a = degree%360;
                if(a == 0){
                    degree = 360;
                }else{
                    degree = a;
                }               
            }
            var radian = degree/180;          
            if(Number.isInteger(radian)){

            }else{
                radian = radian.toFixed(2);
            }          
            return radian;
        }

        function convertDegree(radian) {
            var degree = radian*180;
            if(Number.isInteger(degree)){

            }else{
                degree = degree.toFixed(2);
            }          
            return degree;
        }

        degree.oninput = function(){
            if(this.value == ""){
                radian.value = "";
            }else{
                radian.value = convertRadian(parseInt(this.value)) + "π";
            }            
        }

        radian.oninput = function(){
            if(this.value == ""){
                degree.value = "";
            }else{
                degree.value = convertDegree(this.value);
            }            
        }
    </script>
</body>
</html>
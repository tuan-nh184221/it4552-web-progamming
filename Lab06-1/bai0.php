<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "business_service";

// Create connection
$conn = new mysqli($servername, $username, $password,$database);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}else{
    echo "connected";
}

// Create database
// $sql = "CREATE DATABASE business_service";
// if ($conn->query($sql)) {
//   echo "Database created successfully";
// } else {
//   echo "Error creating database: " . $conn->error;
// }

// sql to create table
// $sql = "CREATE TABLE Businesses (
//     BusinessID INT AUTO_INCREMENT,
//     Name VARCHAR(30) NOT NULL,
//     Address VARCHAR(30) NOT NULL,
//     City VARCHAR(30) NOT NULL,
//     Telephone VARCHAR(10) NOT NULL,
//     URL VARCHAR(50),
//     PRIMARY KEY(BusinessID)
//     )";
    
// if ($conn->query($sql)) {
//     echo "Table Businesses created successfully";
// } else {
//     echo "Error creating table Businesses: " . $conn->error;
// }

// sql to create table
// $sql = "CREATE TABLE Categories (
//     CategoryID INT NOT NULL AUTO_INCREMENT,
//     Title VARCHAR(30) NOT NULL,
//     Description VARCHAR(100) NOT NULL,
//     PRIMARY KEY(CategoryID)
//     )";
    
// if ($conn->query($sql)) {
//     echo "Table Categories created successfully";
// } else {
//     echo "Error creating table Categories: " . $conn->error;
// }

// sql to create table
$sql = "CREATE TABLE Biz_Categories (
    BusinessID INT,
    CategoryID INT,
    PRIMARY KEY(BusinessID, CategoryID)
    )";
    
if ($conn->query($sql)) {
    echo "Table Biz_Categories created successfully";
} else {
    echo "Error creating table Biz_Categories: " . $conn->error;
}

$conn->close();
?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Joe's Cabinet</title>
    <style>
        h1{
            text-align: center;
        }
        p{
            margin: 0.25in;
            border: 0.25in solid black;
            padding: 0.25in;
            background: url(bond-paper.jpeg);
        }
        body{
            background: url(1045.jpg);
            background-repeat: no-repeat;
            background-size: cover;
        }
        .banner{
            background: url(brick.jpeg) repeat-x;
            font-size: 50pt;
            font-family: Arial;
            color:  #e6e6e6;
        }
    </style>
</head>
<body>
    <center>
        <table width=360 height=199>
            <tr><td align="center" class="banner">Joe's Cabinet</td></tr>
        </table>
    </center>
    <p>
        The <b>margin</b> is where bricks show through.
        The <b>border</b> is the solid black line.
        The <b>padding</b> is the space around the text in ther bond-paper background.
    </p>
    <h1>Welcome to cabinet. We specialize in</h1>
    <ul>
        <li>Custom Cabinets</li>
        <li>Kitchen Remodeling</li>
    </ul>
</body>
</html>
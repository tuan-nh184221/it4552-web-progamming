<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
        <h1>Information form</h1>
        <table>
            <tr>
                <td>Email Address: </td>
                <td><input type="text", size="30" name="email"></td>
            </tr>

            <tr>
                <td>URL address: </td>
                <td><input type="text", size="30" name="url"></td>
            </tr>

            <tr>
                <td>Phone number: </td>
                <td><input type="text", size="30" name="phonenumber"></td>
            </tr>

            <tr>
                <td align="right"><input type="submit" name="submit" value="Check validation"></td>
                <td align="light"><input type="reset" value="Reset"></td>
            </tr>
        </table>
    </form>

    <?php
   
        if(isset($_POST['submit'])){
            $email = $_POST["email"];
            $url = $_POST["url"];
            $phone = $_POST["phonenumber"];
        
            $regex_mail = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
            if (preg_match($regex_mail, $email)) {
                echo "Valid email, email = $email<br/>";
            }else{
                echo "Invalid email: $email<br/>";
            }
        
            $regex_url = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
            if ( preg_match($regex_url, $url) ) {
                echo "Valid URL, URL = $url <br>";
            } else {
                echo "Invalid URL: $url <br>";
            }
        
            $regex_phone = '/^[0-9]{10}+$/';
            if(preg_match($regex_phone, $phone)){
                echo "Valid phone number, phone number = $phone<br/>";
            }else{
                echo "Invalid phone number: $phone<br/>";
            }
        }

    ?>
</body>
</html>
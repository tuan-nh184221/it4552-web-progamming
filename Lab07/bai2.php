<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <form action="" method="post">
        <h1>Check Date</h1>
        <table>
            <tr>
                <td>Enter a data to check (mm/dd/yyyy): </td>
                <td><input type="text", size="15" name="date"></td>
            </tr>

            <tr>
                <td align="right"><input type="submit" name="submit" value="Check validation"></td>
                <td align="light"><input type="reset" value="Reset"></td>
            </tr>
        </table>

    </form>

    <?php
        if(isset($_POST['submit'])){
            $date = $_POST["date"];
            $two = '[[:digit:]]{2}';
            $month = '[0-1][[:digit:]]';
            $day = '[0-3][[:digit:]]';
            $year = "[0-2][[:digit:]]$two";

            if (mb_ereg("^($month)/($day)/($year)$", $date)){
                print "Valid date = $date <br />" ;
            }else{
                print "Invalid date = $date <br />" ;
            }
        }
    ?>
</body>
</html>
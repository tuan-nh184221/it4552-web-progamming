<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>  
    <?php
        function checkUserAndComment(){
            if(isset($_POST['submit'])){
                if($_POST['user'] != null || isset($_POST['checkname'])){
                    if($_POST['comment'] != null){
                        if(isset($_POST['checkname'])){
                            $user = 'Ẩn danh';
                        }else{
                            $user = $_POST['user'];
                        }
                        $comment = $_POST['comment'];
        
                        $myfile = fopen('./posts/'.date('YmdHis').'.txt', "w");
    
                        $txt = $user."\n".$comment;
                        fwrite($myfile, $txt);
                        
                        fclose($myfile);
                    }
    
                    return 1;
                }else{
                    return 0;
                }       
            }
        }

        function checkimg(){
            if (isset($_POST['submit']))
            {
                // Nếu người dùng có chọn file để upload
                if ($_FILES['anh']['name'] != null)
                {
                    // Nếu file upload không bị lỗi,
                    // Tức là thuộc tính error > 0
                    if ($_FILES['anh']['error'] > 0)
                    {
                        echo 'File Upload Bị Lỗi';
                    }
                    else{
                        // Upload file
                        $temp = explode(".", $_FILES["anh"]["name"]);
                        $newfilename = date('YmdHis'). '.' . end($temp);
                        move_uploaded_file($_FILES['anh']['tmp_name'], './posts/'.$newfilename);
                    }
                }
            }
        }

        function printMessage(){
            $readfodername = "./posts";
            $result = scandir($readfodername);
            $arrayfile = array_diff($result, array('.', '..'));
            $message = array();
            $eachuser = array('name'=>'','comment'=>'','img'=>null);
            foreach($arrayfile as $value){
                $temp = explode(".", $value);
                if(end($temp) != 'txt'){
                    $filename = './posts/'.$value;
                    $eachuser['img'] = $filename;
                }else{
                    $filename = './posts/'.$value;
                    $fp = fopen($filename,"r");
                    while(! feof($fp)) {
                        $eachuser['name'] = fgets($fp);
                        $eachuser['comment'] = fgets($fp);
                        array_push($message,$eachuser);
                    }
                    $eachuser['img'] = null;
                }
            }

            if(count($message) == 0){
                echo "Chưa có thông điệp nào được đăng";
            }else{
                foreach($message as $value){
                    print '<div style="overflow: auto;width:40%; background-color: pink; border : 1px solid black; padding: 10px; margin-bottom: 10px">';
                    if($value['img'] != null){
                        print '<img style="width:30%; float:left;margin-right:30px" src="'.$value['img'].'">';
                    }
                    print $value['name'].' says : '.$value['comment'].'</div>';
                }
            }
        }

        if(checkUserAndComment()){
            $thongbao = "Đăng thành công";
        }else{
            $thongbao = "Đăng thất bại";
        }
        checkimg();
    ?>

    <h1>Message Board</h1>
    <div style="margin-bottom: 50px">
        <?php printMessage(); ?>
    </div>
   
    <?php if(isset($thongbao)){print'<h2 style="color:red;">'.$thongbao.'</h2>';} ?>
    <div style="border: 1px solid black; width: 40%;padding: 10px">
        <h2>Đăng tải một thông điệp mới</h2>
        <form action="" method="post" enctype="multipart/form-data">
            <p>Tên người dùng</p>
            <input type="text" name ="user" placeholder="Nhập tên">
            <?php  
                if(isset($_POST['submit'])){
                    if($_POST['user'] == null && !isset($_POST['checkname'])){
                        print'<h4 style="color:red">Chưa nhập thông tin</h4>';
                    }
                } 
            ?>
            <br><input type="checkbox" name="checkname" value="true">Đăng thông điệp vô danh
            <p>Nội dung thông điệp</p>
            <input style="width:80%; height:120px;" type="textarea" name="comment" placeholder="Nhập nội dung"><br>
            <?php  
                if(isset($_POST['submit'])){
                    if($_POST['comment'] == null){
                        print'<h4 style="color:red">Chưa nhập thông tin</h4>';
                    }
                } 
            ?>
            <p>Hình ảnh kèm theo(tùy chọn)</p>
            <input type="file" name="anh">
            
            <div style="margin-top : 20px">
            <button type="submit" name="submit">Đăng thông điệp</button>
            </div>
        </form>
    </div>
</body>
</html>